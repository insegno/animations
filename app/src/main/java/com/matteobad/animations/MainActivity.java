package com.matteobad.animations;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    public void fade(View view) {
        ImageView gokuSSJ = (ImageView) findViewById(R.id.gokuSSJImageView);
        ImageView gokuGod = (ImageView) findViewById(R.id.gokuGodImageView);

        gokuSSJ.animate().alpha(0f).setDuration(5000);
        gokuGod.animate().alpha(1f).setDuration(5000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
